# K8s Deployment

Up to date manifest here : https://gitlab.com/volterra-projects/volterra-aks-gke-vk8s-application

In this repo, you can find the different manifest to deploy Sentence in K8S/AKS/GKE/AKS and F5 vk8s.

## List of manifest

* aks-sentence-deployment-nginx-nolb.yaml : deploy frontend nginx with cluster IP
* aks-sentence-deployment-nginx.yaml : deploy frontend nginx with cluster IP and LB
* aks-sentence-deployment.yaml : deploy all Sentence microservices (Locations, generator, colors ...)
* aks-vs-route* : Nginx Ingress Controller - Virtual Server routes (for microservices (api) and frontend)
* aks-ingress-vs-master.yaml : main Ingress Virtual Server to route packets to VS Routes
