# Install Sentence App Using Helm

## Install Sentence App using helm chart:

- Clone this repo. For example `git clone https://gitlab.com/sentence-app/k8s-deployment.git`
- Make any modifications to `./sentence-app/Chart.yaml` and `./sentence-app/values.yaml` as required.
- Run `helm install sentence-app ./sentence-app`
    - If you want to install direct from the tgz package stored in this repo, without cloning the repo, see [Example of installing helm package from this gitlab repo](##Example-of-installing-helm-package-from-this-gitlab-repo).

## Upgrade Sentence App using helm chart:

- Make changes to the configuration as required.
- Run `helm upgrade sentence-app ./sentence-app`.

## Uninstall Sentence App using helm chart:

- Run `helm uninstall sentence-app`.

## View the generated/deployed manifests:

- Run `helm get manifest sentence-frontend`.

## Package the chart into tgz

- Run `helm package <helm root folder>`.

## Example of pushing packaged tgz to package registry in this gitlab repo

- Example: `helm repo add --username userxyz --password glpat-xyzabc123abcxyz K8s-deployment https://gitlab.com/api/v4/projects/31458926/packages/helm/development`.
    - Syntax `helm repo add --username <username> --password <personal_access_token> project-1 https://gitlab.com/api/v4/projects/<project_id>/packages/helm/<channel>`.
- `helm plugin install https://github.com/chartmuseum/helm-push`.
    - Required if `helm cm-push` command produces `unknown command` error.
- `helm cm-push sentence-app-0.3.0.tgz K8s-deployment`.

## Example of installing helm package from this gitlab repo
- `helm repo add --username userxyz --password glpat-xyzabc123abcxyz K8s-deployment https://gitlab.com/api/v4/projects/31458926/packages/helm/development`.
    - Before running this command, you can check if the repo is already added with `helm repo list`.
- `helm repo update`.
    - Pulls the latest updates from the repo.
- `helm install sentence-app K8s-deployment/sentence-app`.
    - Basic install.
- `helm install --set adjectives.serviceType=NodePort --set global.namespace=another-namespace sentence-app K8s-deployment/sentence-app`.
    - Install and over-ride the default variables defined in ./values.yaml, using the `--set` flag.

## Troubleshooting Tip

If you are struggling to find the correct syntax to identify the helm repo in gitlab, you can use a little trial and error to try to find the index.yaml, which will give you the information you need.  For example: `curl -v https://gitlab.com/api/v4/projects/31458926/packages/helm/development/index.yaml` or `curl -v https://gitlab.com/api/v4/projects/31458926/packages/helm/main/index.yaml`.

`---
apiVersion: v1
entries:
  sentence-app:
  - name: sentence-app
    type: application
    version: 0.1.0
    apiVersion: v2
    appVersion: 0.1.0
    description: A Helm chart installation of the Sentence App
    created: '2023-02-22T20:25:32.781054000Z'
    digest: 
    urls:
    - charts/sentence-app-0.1.0.tgz
generated: '2023-02-28T16:31:08.025671296Z'
serverInfo:
  contextPath: "/api/v4/projects/31458926/packages/helm"`